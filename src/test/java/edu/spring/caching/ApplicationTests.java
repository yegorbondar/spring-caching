package edu.spring.caching;

import edu.spring.caching.service.WeatherLookupService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTestConfiguration.class)
@WebAppConfiguration
public class ApplicationTests {

    @Autowired
    WeatherLookupService lookupService;

    @Ignore
    @Test
    public void pull() {
        WeatherLookupService.Weather weather = lookupService.pull("Kharkiv,ua%26units=metric");
        lookupService.pull("Kharkiv,ua%26units=metric");
        lookupService.pull("Kharkiv,ua%26units=metric");
    }

}
