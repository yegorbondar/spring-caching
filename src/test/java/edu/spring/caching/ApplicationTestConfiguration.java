package edu.spring.caching;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.support.CompositeCacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@EnableCaching
@Configuration
@ComponentScan("edu.spring.caching")
public class ApplicationTestConfiguration {
    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    CacheManager cacheManager() throws Exception {
        CompositeCacheManager cacheManager = new CompositeCacheManager();
        cacheManager.setCacheManagers(Arrays.asList((CacheManager)new NoOpCacheManager()));
        cacheManager.setFallbackToNoOpCache(true);
        return cacheManager;
    }

}
