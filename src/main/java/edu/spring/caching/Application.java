package edu.spring.caching;

import com.hazelcast.config.Config;
import com.hazelcast.config.EntryListenerConfig;
import com.hazelcast.config.MapConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;
import edu.spring.caching.listener.CacheEntryEventListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Configuration
@ComponentScan("edu.spring.caching")
@EnableCaching
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    CacheManager cacheManager(HazelcastInstance hazelcastInstance) throws Exception {
        return new HazelcastCacheManager(hazelcastInstance);
    }

    @Bean
    HazelcastInstance hazelcastInstance() throws Exception {
        Config hzConfig = new Config();
        MapConfig weatherPull = new MapConfig();
        weatherPull.setName("weather.pull");
        weatherPull.setEntryListenerConfigs(Arrays.asList(
            new EntryListenerConfig("edu.spring.caching.listener.CacheEntryEventListener", false, true)));
        hzConfig.getMapConfigs().put("weather.pull", weatherPull);
        hzConfig.setProperty( "hazelcast.logging.type", "slf4j");
        return Hazelcast.newHazelcastInstance(hzConfig);
    }

}
