package edu.spring.caching.controller;

import edu.spring.caching.service.WeatherLookupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

@RestController
@Scope("request")
@RequestMapping("/weather")
public class WeatherController {

    private static Logger LOG = LoggerFactory.getLogger(WeatherController.class);

    @Autowired
    private WeatherLookupService lookupService;

    @RequestMapping(value = "/pull", method = RequestMethod.GET)
    public WeatherLookupService.Weather pull(@RequestParam String query){
        return lookupService.pull(query);
    }

    @RequestMapping(value = "/fetch", method = RequestMethod.GET)
    public String fetch(@RequestParam String query){
        return lookupService.fetch(query);
    }

    @RequestMapping(value = "/refreshPull", method = RequestMethod.GET)
    public boolean refreshPull(@RequestParam String query){
        try{
            lookupService.refreshPull(query);
            return true;
        } catch (Exception e){
            LOG.error(e.getMessage(), e);
            return false;
        }
    }

    @RequestMapping(value = "/refreshFetch", method = RequestMethod.GET)
    public boolean refreshFetch(@RequestParam String query){
        try{
            lookupService.refreshFetch(query);
            return true;
        } catch (Exception e){
            LOG.error(e.getMessage(), e);
            return false;
        }
    }

    @RequestMapping(value = "/putPull", method = RequestMethod.GET)
    public WeatherLookupService.Weather putPull(@RequestParam String query){
        return lookupService.putPull(query);
    }

    @RequestMapping(value = "/putFetch", method = RequestMethod.GET)
    public String putFetch(@RequestParam String query){
        return lookupService.putFetch(query);
    }
}
