package edu.spring.caching.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.cache.annotation.CacheRemove;
import javax.cache.annotation.CacheResult;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Service
@Scope("request")
public class WeatherLookupService {

    private static Logger LOG = LoggerFactory.getLogger(WeatherLookupService.class);

    @Value("${openweather.api.url}")
    private String url;

    @Value("${openweather.api.key}")
    private String apiKey;

    @Autowired
    private RestTemplate restTemplate;

    @CacheResult(cacheName = "weather.fetch")
    public String fetch(String query){
        return runFetch(query);
    }

    @Cacheable(value = "weather.pull")
    public Weather pull(String query){
        return runPull(query);
    }

    @CacheRemove(cacheName="weather.fetch")
    public void refreshFetch(String query){
        runFetch(query);
    }

    @CacheEvict(value="weather.pull", allEntries = true)
    public void refreshPull(String query){
        runPull(query);
    }

    @javax.cache.annotation.CachePut(cacheName="weather.fetch")
    public String putFetch(String query){
        return runFetch(query);
    }

    @CachePut(value = "weather.pull")
    public Weather putPull(String query){
        return runPull(query);
    }

    String runFetch(String query){
        long start = System.currentTimeMillis();
        String result = restTemplate.exchange(url + query, HttpMethod.GET, buildRequest(),
            String.class).getBody();
        LOG.info("Request is taken: " + (System.currentTimeMillis() - start));
        return result;
    }

    Weather runPull(String query){
        long start = System.currentTimeMillis();
        Weather result = restTemplate.exchange(url + query, HttpMethod.GET, buildRequest(), Weather.class).getBody();
        long execTime;
        LOG.info("Request is taken: " + (execTime = (System.currentTimeMillis() - start)));
        result.setExecutionTime(execTime);
        return result;
    }

    private HttpEntity<Object> buildRequest(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("x-api-key", apiKey);
        return new HttpEntity<Object>(httpHeaders);
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Weather implements Serializable{
        private long executionTime;
        private long id;
        private long cod;
        private String name;
        private Map<String, String> main = new HashMap<String, String>();

        public long getExecutionTime() {
            return executionTime;
        }

        public void setExecutionTime(long executionTime) {
            this.executionTime = executionTime;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getCod() {
            return cod;
        }

        public void setCod(long cod) {
            this.cod = cod;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Map<String, String> getMain() {
            return main;
        }

        public void setMain(Map<String, String> main) {
            this.main = main;
        }

        @Override
        public String toString() {
            return "Weather{" +
                "executionTime=" + executionTime +
                ", id=" + id +
                ", cod=" + cod +
                ", name='" + name + '\'' +
                ", main=" + main +
                '}';
        }
    }

}
