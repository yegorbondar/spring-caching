package edu.spring.caching.listener;


import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.EntryListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CacheEntryEventListener implements EntryListener {

    public static Logger LOG = LoggerFactory.getLogger(CacheEntryEventListener.class);

    @Override
    public void entryAdded(EntryEvent event) {
        LOG.info("EntryAdded:-Key: " + event.getKey() + " -Value: " + event.getValue() + " -CacheName: " + event.getName());
    }

    @Override
    public void entryRemoved(EntryEvent event) {
        LOG.info("EntryRemoved:-Key: " + event.getKey() + " -Value: " + event.getValue() + " -CacheName: " + event.getName());
    }

    @Override
    public void entryUpdated(EntryEvent event) {
        LOG.info("EntryUpdated:-Key: " + event.getKey() + " -Value: " + event.getValue() + " -CacheName: " + event.getName());
    }

    @Override
    public void entryEvicted(EntryEvent event) {
        LOG.info("EntryEvicted:-Key: " + event.getKey() + " -Value: " + event.getValue() + " -CacheName: " + event.getName());
    }
}
